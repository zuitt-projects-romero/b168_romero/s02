<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>S02: Repetition Control Structures and Array manipulation Activity</title>
</head>
<body>

	<h2>Divisible By 5</h2>
	<?php divisibleByFive(); ?>

	<h2>Printing the names added so far in the student array</h2>
	<pre><?php print_r($sortedNames); ?></pre>

	<h2>Count of the arrays</h2>

	<pre><?php echo count($sortedNames); ?></pre>

	<h2>Adding a student in the last index</h2>
    <?php array_push($studentNames, 'Jane Smith'); ?>

    <pre><?php print_r($studentNames); ?></pre>

    <h2>Count of the arrays</h2>

	<pre><?php echo count($sortedNames); ?></pre>

	<h2>Removing the first student</h2>
	<?php array_shift($studentNames); ?>
	
	<pre><?php print_r($studentNames); ?></pre>

</body>
</html>