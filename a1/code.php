<?php
//Activity No. 2
// 1 
//printing the numbers that are divisible by 5
function divisibleByFive(){
	for($count = 0; $count <= 1000; $count++){
		if($count % 5 == 0) {
			echo $count. ', ';
		}
		if ($count == 1000) {
			break;
		}
	}
};

// 2, Arrays
// Accept a name of the student and add it to the student array.
// Print the names added so far in the student array.
// Count the number of names in the student array.
// Add Another student then print the name and its count
// Finally remove the first student and print the name and its count

$studentNames = ['John Smith'];

$sortedNames = $studentNames;
$reverseStudentNames = $studentNames;
sort($studentNames);
rsort($reverseStudentNames);
